$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" id="myViewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ

//  window.onload = function () {
// 	if(screen.width <= 617) {
// 	    var mvp = document.getElementById('myViewport');
// 	    mvp.setAttribute('content','width=617');
// 	}
// }



$(document).ready(function(){




		/* ------------------------------------------------
		ANCHOR START
		------------------------------------------------ */
				 
			    function goUp(){
					var windowHeight = $(window).height(),
						windowScroll = $(window).scrollTop();

					if(windowScroll>windowHeight/2){
						$('.arrow_up').addClass('active animated fadeInRight');
					}

					else{
						$('.arrow_up').removeClass('active animated fadeInRight');
						$('.arrow_up').removeClass('click animated fadeOutRight');
					}

			    }

			    goUp();
				$(window).on('scroll',goUp);

				$('.arrow_up').on('click ontouchstart',function () {

					if($.browser.safari){
						$('body').animate( { scrollTop: 0 }, 1100 );
						$('.arrow_up').addClass('click animated fadeOutRight');
					}
					else{
						$('html,body').animate( { scrollTop: 0}, 1100 );
						$('.arrow_up').addClass('click animated fadeOutRight');
					}
					return false;
					
				});

		/* ------------------------------------------------
		ANCHOR END
		------------------------------------------------ */



		/* ------------------------------------------------
		RESPONSIVE MENU START
		------------------------------------------------ */

				$(".resp_btn, .close_resp_menu").on("click ontouchstart", function(){
					$("body").toggleClass("show_menu")
				});

				$(document).on("click ontouchstart", function(event) {
					if ($(event.target).closest("nav,.resp_btn").length) return;
					$("body").removeClass("show_menu");
					if($(window).width() <= 768){
						$(".menu_item").removeClass("active").find(".dropdown_menu").css("display","none");
					}
				    event.stopPropagation();
			    });

				// проверка на наличие элемента и вставка хтмл кода
			 	 // if($(window).width() <= 767){
					if ($(".menu_item").length){
				        if($(".menu_item").has('.dropdown_menu')){
				        	var $this = $(".menu_item"),
				        		dropdownMenu = $this.find(".dropdown_menu");

				        		dropdownMenu.parent($this).find(".menu_link").append('<span class="dropdown_arrow"><span class="dropdown_arrow_item"></span></span>');
				        }
				    }
				// }
				// проверка на наличие элемента и вставка хтмл кода


				$('.menu_link').on('click ontouchstart',function(event){
					if($("html").hasClass("md_no-touch"))return;

			        var windowWidth = $(window).width(),
			            $parent = $(this).parent('.menu_item');
			        if(windowWidth > 768){
			          // if($("html").hasClass("md_touch")){
			            if((!$parent.hasClass('active')) && $parent.find('.dropdown_menu').length){

			              event.preventDefault();

			              $parent.toggleClass('active')
			               .siblings()
			               .find('.menu_link')
			               .removeClass('active');
			            }
			          // }  
			        }
			        
			        else{
			            
			          if((!$parent.hasClass('active')) && $parent.find('.dropdown_menu').length){

			            event.preventDefault();

			            $parent.toggleClass('active')
			             .siblings()
			             .removeClass('active');
			            $parent.find(".dropdown_menu")
			             .slideToggle()
			             .parents('.menu_item')
			             .siblings()
			             .find(".dropdown_menu")
			             .slideUp();
			          }
			        }

			    });

		/* ------------------------------------------------
		RESPONSIVE MENU END
		------------------------------------------------ */



		/* ------------------------------------------------
		FILTER ON CLICK AND CHANGE POSITION START
		------------------------------------------------ */

				//скрипт  считывает сколько ширина скрола начало

				var scrollWidth;
				function detectScrollBarWidth(){
					var div = document.createElement('div');
					div.className = "detect_scroll_width";
					document.body.appendChild(div);
					scrollWidth = div.offsetWidth - div.clientWidth;
					document.body.removeChild(div);
					// console.log(scrollWidth);
				}
				detectScrollBarWidth();

				//скрипт  считывает сколько ширина скрола конец

				// скрипт переставляет блок на определенной ширине в другой блок начало
				
				function filterPosition(){
		          var bodyWidth = $(window).width();
		          if(bodyWidth + scrollWidth <= 767 && $('body').hasClass('filterPosition')){
		            $('#header').prepend($('.collection_container'));
		            $('body').removeClass('filterPosition');
		          }
		          else if(bodyWidth + scrollWidth > 767 && !$('body').hasClass('filterPosition')){
		            $("#content").find('.filter_bx').prepend($('.collection_container'));
		            $('body').addClass('filterPosition');
		          }
		        } 
		        filterPosition();
				$(window).on('resize',function(){
			        setTimeout(function(){
		            	filterPosition();
		            },200);
	            });

				// скрипт переставляет блок на определенной ширине в другой блок конец


		        // проверка и вставка высоты на фильтр при фиксации и разлипании хедера  начало

	        	function containerTop(target) {
			        var blockTop        = $("#header").find(".collection_container"),
			        	headerFixHeight = $(".header_bottom_container").outerHeight();
			        	// console.log(target);
			        if (target == "fix") {
			        	blockTop.css('top', headerFixHeight);
			        }
			        else if (target == "static"){
			        	blockTop.css('top', 0);
			        }
				}
				containerTop();
				$(window).on('scroll',function(){
					if($(".header_bottom_container").hasClass("is-sticky") && !($(".header_bottom_container").hasClass('stickyTrue'))){
						setTimeout(function(){
			            	containerTop("fix");
			            },200);
			           $(".header_bottom_container").addClass('stickyTrue'); 
					}
					else if(!($(".header_bottom_container").hasClass("is-sticky"))){
			        	setTimeout(function(){
			            	containerTop("static");
			            },200); 
			          	$(".header_bottom_container").removeClass('stickyTrue');
					}
				});

		        // проверка и вставка высоты на фильтр при фиксации и разлипании хедера  конец
		        
		        // скрипт который при клике показывает фильтр начало

				if($(window).width() <= 767){
					if($(".system_controller").length){
						$(".system_controller, .filter_close_resp").on("click", function(){
							var body = $("body");
							body.toggleClass("show_filter");
							$(this).toggleClass("current");
						});

						$(document).on("click ontouchstart", function(event) {
							if ($(event.target).closest(".collection_container,.system_controller").length) return;
							$("body").removeClass("show_filter");
							$(".system_controller").removeClass("current");
							event.stopPropagation();
						});
					}
				}

		        // скрипт который при клике показывает фильтр конец


		/* ------------------------------------------------
		FILTER ON CLICK AND CHANGE POSITION END
		------------------------------------------------ */



		/* ------------------------------------------------
		FOCUS INPUT START
		------------------------------------------------ */

			   $(".input_field").live("focus blur", function(e){
			      var $this = $(this),
			          parentInput = $(".search_box");
			      setTimeout(function(){
			        $this.toggleClass("focused_child", $this.is(":focus"));
			        $this.parent(parentInput).toggleClass("focused_parent", $this.is(":focus"));
			      }, 0);
			    });

		/* ------------------------------------------------
		FOCUS INPUT END
		------------------------------------------------ */



		/* ------------------------------------------------
		CHECKBOX START
		------------------------------------------------ */
				

			   if($(".collection_container_checkbox_item").length){

			        	var $this = $(".collection_container_checkbox_item"),
			        		label = $this.find(".click_check"),
			        		showCheckbox = $this.find(".collection_container_checkbox_list_sub");

			        	label.on("click", function(){
			        		label.toggleClass("current_check");
			        		showCheckbox.slideToggle(400);
			        	});

			   } 

		/* ------------------------------------------------
		CHECKBOX END
		------------------------------------------------ */



		/* ------------------------------------------------
		COLOR CURRENT START
		------------------------------------------------ */
		
				if($(".collection_container_checkbox_colors_list_item").length){
					$(".collection_container_checkbox_colors_list_item").on("click", function(){
						$(this).toggleClass("current");
					});
				}

		/* ------------------------------------------------
		COLOR CURRENT END
		------------------------------------------------ */




		/* ------------------------------------------------
		COUNTER START
		------------------------------------------------ */
				if($(".input_quantity_row").length){
						
					$('.minus').click(function () {
		                var $input = $(this).closest('.input_quantity_row').find('.input_quantity');
		                var count = parseInt($input.val()) - 1;
		                count = count < 2 ? 2 : count;
		                $input.val(count);
		                $input.change();
		                return false;
		            });

					var price = parseInt($(".price").text());

		                $(".action").on("click", function(){

		                    var $this         = $(this), //кнопка + и -
		                        parent        = $this.parents(".input_quantity_row"), // родитель главный 
		                        priceV        = parent.find(".price"), // цена с которой работает 
		                        input         = parent.find(".input_quantity"),
		                        inputVal      = parseInt(input.val()); // инпут берет значение

		                    if($this.hasClass("minus")){
		                        input.val(inputVal -= 1); 
		                    }   
		                    else if($this.hasClass("plus")){
		                        input.val(inputVal += 1); 
		                    }

		                    if (parent.find(".cart_prod_parice").length){
		                    	var newPrice = parseInt(parent.find(".cart_prod_parice").text());
		                    	priceV.text( inputVal * newPrice );
		                    }

		                    else{
		                    	priceV.text( inputVal * price );
		                    	
		                    }
		                });
	            }

		/* ------------------------------------------------
		COUNTER END
		------------------------------------------------ */



		/* ------------------------------------------------
		REMOVE TR TABLE ON CLICK START
		------------------------------------------------ */

				$(".offer_close").on("click", function(){
					var $this  = $(this),
						parent = $this.parents(".offers_table_row");
						parent.remove("tr");
				});

		/* ------------------------------------------------
		REMOVE TR TABLE ON CLICK END
		------------------------------------------------ */




		/* ------------------------------------------------
		ШИРИНА БЛОКА ПОКАЗЫВАЕТЬСЯ НА ЭКРАН START
		------------------------------------------------ */

				// var div      = document.createElement('div');
				// 	cssObj   = {"position": "fixed","bottom": "50px","right": "50px","font-size": "20px","color": "black"};
				// div.className = "windowWidth";
				// $(div).css(cssObj);
				// document.body.appendChild(div)


				// function windowWidth(target){
				// 	var widthDiv = $(window).width();
				// 	$("body").find(".windowWidth").text(widthDiv + scrollWidth);
				// 	// console.log(widthDiv);
				// }
				// windowWidth();

				// $(window).on('resize',function(){
				// 	windowWidth();
				// });

        /* ------------------------------------------------
		ШИРИНА БЛОКА ПОКАЗЫВАЕТЬСЯ НА ЭКРАН END
		------------------------------------------------ */
});
