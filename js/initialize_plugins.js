//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var tabs = $('.tabs'),
			sticky = $(".sticky"),
			matchheight = $("[data-mh]"),
		    styler = $(".styler"),
		    main_slider = $(".main_slider"),
		    product_slider = $(".product_slider"),
		    recommend_slider = $(".recommend_slider"),
		    phone = $(".phone"),
			popup = $("[data-popup]"),
			verticalSlider = $(".bxslider"),
			rangeSlider = $("#slider-range"),
			zoomSlider = $("#img_01"),
			fancybox = $(".fancybox"),
			windowW = $(window).width(),
			windowH = $(window).height();


			if(matchheight.length){
					include("plugins/jquery.matchHeight-min.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(tabs.length){
					include("plugins/easy-responsive-tabs/easyResponsiveTabs.js");
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}
			if(main_slider.length || product_slider.length || recommend_slider.length){
					include('plugins/owl-carousel/owl.carousel.js');
			}
			if(rangeSlider.length){
					include('plugins/range-slider/jquery-ui.js');
					include('plugins/range-slider/jquery.ui.touch-punch.min.js');
			}
			if(phone.length){
					include('plugins/maskedInput.js');
			}
			if(zoomSlider.length){
					include('plugins/elevatezoom-master/jquery.elevatezoom.js');
			}
			if(fancybox.length){
					include('plugins/fancybox/jquery.fancybox.js');
			}
			if(verticalSlider.length){
					include('plugins/bx-slider/jquery.bxslider.js');
			}
			if (sticky.length){
					include('plugins/sticky.js');	
					include("plugins/jquery.smoothscroll.js");
			}

					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			/* ------------------------------------------------
			STICKY START
			------------------------------------------------ */


					function stickyHeader(){
							var windowW = $(window).width();

							if(windowW <= 768){
								if (sticky.length && !sticky.data('init')){
									sticky.sticky({
								        topspacing: 0,
								        styler: 'is-sticky',
								        animduration: 0,
								        unlockwidth: false,
								        screenlimit: false,
								        sticktype: 'alonemenu'
									});
									sticky.data('init', true);
								}

							}
							else if(sticky.data('init') && windowW > 768){
								setTimeout(function(){
									$(window).off('scroll.sticky resize.sticky');
									sticky.data('init', false);
								}, 100);
							}

					}
					stickyHeader();
					$(window).on('resize',stickyHeader);


			/* ------------------------------------------------
			STICKY END
			------------------------------------------------ */



			
			/* ------------------------------------------------
			BX-SLIDER START
			------------------------------------------------ */

					if(verticalSlider.length){
						verticalSlider.each(function(){
							var	$this = carousell = $(this),
								parent = $(".vertical_slider_container"),
								closeItem = $this.find(".vertical-close"),
	  			    			product_block = $this.find("li").length;

	  			    		if(product_block > 2){
								verticalSlider.bxSlider({
									mode: 'vertical',
									touchEnabled: true,
									infiniteLoop: false,
									preventDefaultSwipeY: true,
									minSlides: 2,
									maxSlides: 2,
									slideMargin: 0
								});								
								parent.removeClass('current');

								closeItem.on("click", function(){
									var $this = $(this);

									$this.parents("li").remove();

									product_block = carousell.find("li").length;

	  			    				if(product_block <= 2){
										verticalSlider.destroySlider();
										parent.addClass('current');
									}
	  			    				else if(product_block > 2){
										verticalSlider.reloadSlider({
											mode: 'vertical',
											touchEnabled: true,
											infiniteLoop: false,
											preventDefaultSwipeY: true,
											minSlides: 2,
											maxSlides: 2,
											slideMargin: 0
										});
	  			    				}

								});
							}
							else{
								parent.addClass('current');
							}
						});
					}

			/* ------------------------------------------------
			BX-SLIDER END
			------------------------------------------------ */




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							// selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */




			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(main_slider.length || product_slider.length || recommend_slider.length){
				  		main_slider.each(function(){
				  			var	$this = $(this),
				  			    item_img_block = $this.find(".item_img_block").length;

				  			if(item_img_block > 1){

						  		$this.owlCarousel({
									navigation : true,
									slideSpeed : 2000,
						        	pagination : true,
									paginationSpeed : 1500,
									autoPlay: 9000,
									stopOnHover: true,
									singleItem:true,
									items: 1,
							        navigationText: [ '', '' ]
							  	});

				  			}

				  		});

				  		product_slider.owlCarousel({
							navigation : true,
							slideSpeed : 500,
				        	pagination : true,
							paginationSpeed : 1500,
							autoPlay: 9000,
							stopOnHover: true,
							singleItem:false,
							items: 4,
					        navigationText: [ '', '' ],
					        itemsDesktop : [1900, 4],
					        itemsDesktopSmall : [1080, 3],
					        itemsTablet : [768, 2],
					        itemsTabletSmall : [600, 1],
					        itemsMobile : [479, 1]
					  	});

					  	recommend_slider.owlCarousel({
							navigation : true,
							slideSpeed : 500,
				        	pagination : true,
							paginationSpeed : 1500,
							autoPlay: 9000,
							stopOnHover: true,
							singleItem:false,
							items: 3,
					        navigationText: [ '', '' ],
					        itemsDesktop : [1900, 3],
					        itemsDesktopSmall : [991, 2],
					        itemsTablet : [768, 2],
					        itemsTabletSmall : [600, 1],
					        itemsMobile : [479, 1]
					  	});
					  	
	  			
				  	}


			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */




			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(tabs.length){
						tabs.easyResponsiveTabs();

					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */




			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal();
						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */

			/* ------------------------------------------------
			RANGE-SLIDER START
			------------------------------------------------ */

					if(rangeSlider.length){
						rangeSlider.slider({
							touch: true,
							range: true,
							min: 700,
							max: 2000,
							values: [ 700, 1500 ],
							slide: function( event, ui ) {
								$( "#amount1" ).val( "от" + " " + ui.values[ 0 ] + " " + "грн." );
								$( "#amount2" ).val( "до" + " " + ui.values[ 1 ] + " " + "грн." );
							}
						});  
					    $( "#amount1" ).val( "от" + " " + $(rangeSlider).slider( "values", 0 ) + " " + "грн." );
					    $( "#amount2" ).val( "до" + " " + $(rangeSlider).slider( "values", 1 ) + " " + "грн." );

						rangeSlider.slider();  
				    }

			/* ------------------------------------------------
			RANGE-SLIDER END
			------------------------------------------------ */

			/* ------------------------------------------------
			ZOOM-SLIDER START
			------------------------------------------------ */
					if(zoomSlider.length){
						zoomSlider.elevateZoom({
							responsive : true,
							// constrainType:"height", 
							// constrainSize:274, 
							zoomType: "lens", 
							containLensZoom: true, 
							gallery:'gallery', 
							cursor: 'pointer', 
							galleryActiveClass: "active"
						});

		                zoomSlider.bind("click", function(e) { 
		                    var ez = $(zoomSlider).data('elevateZoom');   
		                    $.fancybox(ez.getGalleryList()); return false; 
		                });
	                }
					
			/* ------------------------------------------------
			ZOOM-SLIDER END
			------------------------------------------------ */




			/* ------------------------------------------------
			MASKEDINPUT START
			------------------------------------------------ */

					if(phone.length){
				        // $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
				        $(".phone").mask("+38(999) 999-99-99");
					}

			/* ------------------------------------------------
			MASKEDINPUT END
			------------------------------------------------ */
		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
